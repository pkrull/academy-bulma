# Assignment - Elements and Modifiers

This assignment is meant to get used reading the Bulma documentation.  We will be using the [Elements](https://bulma.io/documentation/elements/) and [Modifiers](https://bulma.io/documentation/modifiers/) for this assignment.

If you like having a background on your page, start off using of these HTML structures.  We will be looking [Layouts](https://bulma.io/documentation/layout/) in the near future.

```html
    <section class="section has-background-dark">
        <!-- Other code here. Background will be dark and the height is determined by the space needed -->
    </section>

    <section class="hero is-fullheight is-dark">
        <div class="hero-body">
            <!-- Other code here. The entire background will be dark. -->
        </div>
    </section>
```
Part I - Headers with color, size and other modifiers

```html
    <div>
        <h1>Hello World</h1>
    </div>
```
* Add the `.box` class to the `<div>`
* Add the `.title` class to the `<h1>`
* Change the `<h1>` title class size to 1
* Change the `<h1>` font color to red
* Center the text of the `<h1>`
* Make the `World` text bold an uppercase
* Right align the `<h1>` on desktop only

Part II - Responsive modifiers

Bulma is a Mobile first framework.  We can change the way elements are displayed based on the screen width. We are going to look at how we can hide/show elements based on the device being used.

Using the below HTML, apply the necessary classes so that as the screen changes size, only the correct div will be displayed.

```html
    <section class="section">
        <div class="box has-background-success">
            <p>I will only be visible on Mobile</p>
        </div>
        <div class="box has-background-success">
            <p>I will only be visible on Tablet</p>
        </div>
        <div class="box has-background-success">
            <p>I will only be visible on Desktop</p>
        </div>
        <div class="box has-background-success">
            <p>I will only be visible on Widescreen</p>
        </div>
    </section>
```
Part III - Other popular Elements

For this challenge we are going to use the following:

* [Bulma Image](../images/bulma-icon.png)
* [Font Awesome](https://fontawesome.com/)
* `.notification`
* `.image`
* `.icon`
* `.delete`
* `.has-text-success`
* `.has-text-danger`
* `.is-128x128`

Reproduce this image

![Elements and Modifiers](../images/elements-modifiers.png)

[<---- Elements](https://gitlab.com/pkrull/academy-bulma/blob/master/lessons/04.Elements.md)