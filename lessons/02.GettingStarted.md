# Let's get started

### Bulma provides us with a HTML starter template.

Create a `practice_Bulma/` directory and create a bulma.html file with the below code.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hello Bulma!</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
  </head>
  <body>
  <section class="section">
    <div class="container">
      <h1 class="title">
        Hello World
      </h1>
      <p class="subtitle">
        My first website with <strong>Bulma</strong>!
        <span class="icon">
            <i class="far fa-thumbs-up"></i>
        </span>
      </p>
    </div>
  </section>
  </body>
</html>
```

[Alternate ways to use Bulma can be found here](https://bulma.io/documentation/overview/start/)

### <ins>Lets take a look at what we have</ins>
1. For Bulma to work correctly, we need to set up our page to be responsive
```html
    <!DOCTYPE html>
```
2. Next we add the responsive viewport meta tag
```html
    <meta name="viewport" content="width=device-width, initial-scale=1">
```
3. We import bulma and fontawesome which allows to to use [FontAwesome icons](https://fontawesome.com/)
```html
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
```

### Now that we have a working Bulma, lets see what Bulma can do for us

[<---- Introduction](https://gitlab.com/pkrull/academy-bulma/blob/master/lessons/01.Introduction.md) &nbsp;&nbsp;&nbsp;&nbsp; [Modifiers ---->](https://gitlab.com/pkrull/academy-bulma/blob/master/lessons/03.Modifiers.md)