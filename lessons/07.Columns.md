# Columns

### A quick and easy way to create a responsive layout

Columns are powered with [FlexBox](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox)

[Place holder for FlexBox intro]

### Basics

A column layout requires 2 classes:
* `.columns` for a container
* As many `.column` as needed, each will default to equal widths

```html
    <div class="columns">
        <div class="column">Column One</div>
        <div class="column">Column Two</div>
        <div class="column">Column Three</div>
        <div class="column">Column Four</div>
        <div class="column">Column Five</div>
    </div>
```

### Sizes

Using the following classes, you can set the `width` of a column in multiple ways:
* `.is-three-quarters`
* `.is-two-thirds`
* `.is-half`
* `.is-one-third`
* `.is-one-quarter`
* `.is-full`

or in muliples of 20:
* `.is-four-fifths`
* `.is-three-fifths`
* `.is-two-fifths`
* `.is-one-fifth`

or even with the 12 column system
* `.is-1` ... `.is-12`

Any column `not` with a set width will be determined and equal with the remaining space

```html
    <div class="columns">
        <div class="column is-half">My width is half of the screen</div>
        <div class="column">My width is quarter of the screen</div>
        <div class="column">My width is quarter of the screen</div>
    </div>
```

### Responsiveness

Columns are activated from tablet onward, meaning columns are stacked vertically on mobile.  To maintain the horizontal stacking, use the `.is-mobile` modifier with the `.columns` class.

You can also define the behaviour of the columns based on the viewport size using the following modifiers:
* `.is-*-mobile`
* `.is-*-tablet`
* `.is-*-desktop`
* `.is-*-widescreen`
* `.is-*-fullhd`

### Nesting

Columns can also be nested:

* parent `.columns`
    * child `.column`
        * child `.columns`
            * child `.column` ...

```html
    <div class="columns">
        <div class="column">
            <h1 class="title box has-background-link">Column One</h1>
            <div class="columns">
                <div class="column">
                    <p class="box has-background-link">Nested Column One</p>
                </div>
                <div class="column">
                    <p class="box has-background-link">Nested Column Two</p>
                </div>
                <div class="column">
                    <p class="box has-background-link">Nested Column Three</p>
                </div>
            </div>
        </div>
        <div class="column">
            <h1 class="title box has-background-success">Column Two</h1>
            <div class="columns">
                <div class="column">
                    <p class="box has-background-success">Nested Column One</p>
                </div>
                <div class="column">
                    <p class="box has-background-success">Nested Column Two</p>
                </div>
                <div class="column">
                    <p class="box has-background-success">Nested Column Three</p>
                </div>
            </div>
        </div>
    </div>
```

### Gap

Columns of a default gap setting of `0.75em`, this can be changed with the following modifiers:

* `.is-gapless`: removes gap
* Variable gap using `.is-variable` with the `.columns` container
    * `.is-0`: same as is-gapless through `.is-8`

[More on Columns](https://bulma.io/documentation/columns/)

[<---- Forms](https://gitlab.com/pkrull/academy-bulma/blob/master/lessons/06.Forms.md) &nbsp;&nbsp;&nbsp;&nbsp; [Layouts ---->](https://gitlab.com/pkrull/academy-bulma/blob/master/lessons/08.Layouts.md)

[Columns Assignment](https://gitlab.com/pkrull/academy-bulma/blob/master/lessons/07.assignment-columns.md)